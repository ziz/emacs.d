;;; devesine-osx.el --- OS X configuration
;;; Commentary:
;;; Code:

;; Command is meta
(setq mac-command-modifier 'meta)
;; Left option is super
(setq mac-option-modifier 'super)
;; Right option is compositional
(setq mac-right-option-modifier nil)

;; Don't open new frames for outside file open requests
(setq ns-pop-up-frames nil)

;; Enable full-screen toggle
(global-set-key (kbd "M-RET") 'ns-toggle-fullscreen)

;; Allow editing of binary .plist files.
(add-to-list 'jka-compr-compression-info-list
             ["\\.plist$"
              "converting text XML to binary plist"
              "plutil"
              ("-convert" "binary1" "-o" "-" "-")
              "converting binary plist to text XML"
              "plutil"
              ("-convert" "xml1" "-o" "-" "-")
              nil nil "bplist"])

;; Update is needed after changing the list
(jka-compr-update)


;; Swap the cmd-` and option-` bindings (so cmd-` continues to be "other-frame")
(let ((superkey (global-key-binding (kbd "s-`")))
      (metakey (global-key-binding (kbd "M-`"))))
  (global-set-key (kbd "s-`") metakey)
  (global-set-key (kbd "M-`") superkey))

(provide 'devesine-osx)

;;; devesine-osx.el ends here
