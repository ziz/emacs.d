;;; devesine-packages.el --- Automatic package installation handling
;;; Commentary:
;;; Code:

(with-no-warnings
  (require 'cl))
(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
      (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

(setq package-enable-at-startup nil)
(package-initialize)

;; required because of a package.el bug
(with-no-warnings
  (setq url-http-attempt-keepalives nil))

(defvar devesine-autoinstall-packages
  '(
    expand-region ;; like vim inside mode
    magit ;; git interaction
    rainbow-mode ;; colorize color language like red blue green #abc
    volatile-highlights ;; highlight the effect of actions briefly
    yasnippet ;; snippet support
    angular-snippets ;; yasnippets for angular.js
    zenburn-theme zen-and-art-theme ;; themes
    mmm-mode ;; Multiple Major Modes
    scss-mode ;; SCSS files - i never edit sass files
    paredit rainbow-delimiters ;; for editing lispy things
    js2-mode ;; melpa provides the mooz branch
    ;;cider
    ;;4clojure
    ;;clojure-mode ;; gotta start on clojure some time
    cperl-mode
    ;; ruby-tools inf-ruby yari ruby-end ruby-block ;; yay, ruby
    markdown-mode ;; because SF uses it a lot
    yaml-mode ;; saltstack uses yaml
    jinja2-mode ;; saltstack uses jinja
    pandoc-mode ;; it's occasionally useful
    flycheck ;; flycheck-mode
    flyspell-lazy ;; improve flyspell performance
    flymake
    flymake-yaml
    ;;flymake-ruby
    ;;flymake-sass
    flymake-shell
    flymake-cursor
    idle-highlight-mode
    deft
    unicode-fonts
    undo-tree
    goto-chg
    diminish
    anaconda-mode
    highlight-indentation
    company company-c-headers ;; company completion mode
    company-anaconda
    ;; jedi
    )
  "A list of packages to ensure are installed at launch.")

(defun devesine-packages-installed-p ()
  "Return t if all packages listed for autoinstall are installed."
  (loop for p in devesine-autoinstall-packages
	when (not (package-installed-p p)) do (return nil)
	finally (return t)))

(defun devesine-install-packages ()
  "Update package database and install packages if any are missing."
  (unless (devesine-packages-installed-p)
    ;; check for new packages (package versions)
    (message "%s" "Emacs is now refreshing its package database...")
    (package-refresh-contents)
    (message "%s" " done.")
    ;; install the missing packages
    (dolist (p devesine-autoinstall-packages)
      (unless (package-installed-p p)
	(package-install p)))))

(devesine-install-packages)

(provide 'devesine-packages)
;;; devesine-packages.el ends here
