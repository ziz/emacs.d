;; devesine-tramp.el
;; 

;; Tramp

(require 'tramp)

;; Use shared persistent connections regardless of specific file, until tramp blows up and doesn't work anymore because of it
(add-to-alist 'tramp-methods 
              '("scpcm"
               (tramp-login-program "ssh")
               (tramp-login-args
                (("-l" "%u")
                 ("-p" "%p")
                 ("-o" "ControlPath=~/.ssh/.control/master-%%r@%%h:%%p")
                 ("-o" "ControlMaster=auto")
                 ("-e" "none")
                 ("%h")))
               (tramp-async-args
                (("-q")))
               (tramp-remote-sh "/bin/sh")
               (tramp-copy-program "scp")
               (tramp-copy-args
                (("-P" "%p")
                 ("-p" "%k")
                 ("-q")
                 ("-r")
                 ("-o" "ControlPath=~/.ssh/.control/master-%%r@%%h:%%p")
                 ("-o" "ControlMaster=auto")
		 ("-o" "ControlPersist=no")))
               (tramp-copy-keep-date t)
               (tramp-copy-recursive t)
               (tramp-password-end-of-line nil)
               (tramp-gw-args
                (("-o" "GlobalKnownHostsFile=/dev/null")
                 ("-o" "UserKnownHostsFile=/dev/null")
                 ("-o" "StrictHostKeyChecking=no")))
               (tramp-default-port 22)))

(add-to-alist 'tramp-methods
              '("rsynccm"
                 (tramp-login-program "ssh")
                 (tramp-login-args
                  (("-l" "%u")
                   ("-p" "%p")
                   ("-o" "ControlPath=~/.ssh/.control/master-%%r@%%h:%%p")
                   ("-o" "ControlMaster=yes")
                   ("-e" "none")
                   ("%h")))
                 (tramp-async-args
                  (("-q")))
                 (tramp-remote-sh "/bin/sh")
                 (tramp-copy-program "rsync")
                 (tramp-copy-args
                  (("-t" "%k")
                   ("-r")))
                 (tramp-copy-env
                  (("RSYNC_RSH")
                   ("ssh -o ControlPath=~/.ssh/.control/master-%%r@%%h:%%p -o ControlMaster=auto")))
                 (tramp-copy-keep-date t)
                 (tramp-copy-keep-tmpfile t)
                 (tramp-copy-recursive t)
                 (tramp-password-end-of-line nil)))

;; (setq tramp-default-method "rsynccm")
;; debugging
;(setq tramp-verbose 10)
;(setq tramp-debug-buffer t)
(setq vc-ignore-dir-regexp
                (format "\\(%s\\)\\|\\(%s\\)"
                        vc-ignore-dir-regexp
                        tramp-file-name-regexp))

;; End of Tramp


(provide 'devesine-tramp)
