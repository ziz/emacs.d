;;; devesine-ui.el --- UI customizations that don't make sense elsewhere
;;; Commentary:
;;; Code:

(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(autoload 'inform-mode "inform-mode" "Inform editing mode." t)
(autoload 'inform-maybe-mode "inform-mode" "Inform/C header editing mode.")
(autoload 'LilyPond-mode "lilypond-mode" "LilyPond Editing Mode" t)

;; Don't exit on C-xC-c
;; --------------------------------------------------------
(defun dont-kill-emacs ()
  "Notify instead of killing Emacs."
  (interactive)
  (message (substitute-command-keys "To exit emacs: \\[kill-emacs]")))

(global-set-key "\C-x\C-c" 'dont-kill-emacs)

;; ispell
(setq ispell-program-name "/usr/local/bin/aspell"
      ispell-list-command "list"
      ispell-extra-args '("--sug-mode=ultra"))

(defun devesine-turn-on-flyspell ()
  "Force flyspell-mode on using a positive argument.  For use in hooks."
  (interactive)
  (flyspell-mode +1))

(add-hook 'message-mode-hook 'devesine-turn-on-flyspell)
(add-hook 'text-mode-hook 'devesine-turn-on-flyspell)

;; enable narrowing / expanding commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

(require 'expand-region)

;; bookmarks
(setq bookmark-default-file (expand-file-name "bookmarks" devesine-savefile-dir)
      bookmark-save-flag 1)

;; shorter aliases for ack-and-a-half commands
(defalias 'ack 'ack-and-a-half)
(defalias 'ack-same 'ack-and-a-half-same)
(defalias 'ack-find-file 'ack-and-a-half-find-file)
(defalias 'ack-find-file-same 'ack-and-a-half-find-file-same)


;; uniquify changes conflicting buffer names from file<2> etc
(require 'uniquify)
(setq uniquify-buffer-name-style 'reverse)
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t) ; rename after killing uniquified
(setq uniquify-ignore-buffers-re "^\\*") ; don't muck with special buffers

;; saveplace remembers your location in a file when saving files
(setq save-place-file (expand-file-name "saveplace" devesine-savefile-dir))
;; activate it for all buffers
(setq-default save-place t)
(require 'saveplace)

;; savehist keeps track of some history
(setq savehist-additional-variables
      ;; search entries
      '(search ring regexp-search-ring)
      ;; save every minute
      savehist-autosave-interval 60
      ;; keep the home clean
      savehist-file (expand-file-name "savehist" devesine-savefile-dir))
(savehist-mode t)

;; save recent files
(setq recentf-save-file (expand-file-name "recentf" devesine-savefile-dir)
      recentf-max-saved-items 200
      recentf-max-menu-items 15)
(recentf-mode t)

(setq auto-save-list-file-prefix
      (expand-file-name "auto-save-list/.saves-" devesine-savefile-dir))

;; use shift + arrow keys to switch between visible buffers
(require 'windmove)
(windmove-default-keybindings)

;; Hippie-expand!
(global-set-key (kbd "M-/") 'hippie-expand)
(setq hippie-expand-try-functions-list '(try-expand-dabbrev
                                         try-expand-dabbrev-all-buffers
                                         try-expand-dabbrev-from-kill
                                         try-complete-file-name-partially
                                         try-complete-file-name
                                         try-expand-all-abbrevs
                                         try-expand-list
                                         try-expand-line
                                         try-complete-lisp-symbol-partially
                                         try-complete-lisp-symbol))

;; smart pairing for all
(electric-pair-mode t)

;; show-paren-mode: subtle highlighting of matching parens (global-mode)
(show-paren-mode +1)
(setq show-paren-style 'parenthesis)

;; emacs defaults
(setq column-number-mode t)
(setq line-number-mode t)
(setq size-indication-mode nil)
(setq inhibit-startup-screen t)
(blink-cursor-mode -1)
(defun iconify-or-deiconify-frame nil "Don't iconify frames.") ;; C-z shouldn't iconify frames
(scroll-bar-mode -1) ;; Scrollbars are silly
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1)) ;; as are toolbars
(auto-compression-mode 1)
(auto-image-file-mode 1)

;; Yay, less typing
(if (version<= emacs-version "23.1")
    (iswitchb-mode))
(require 'ido)
(ido-mode t)

(setq ido-enable-prefix nil
      ido-enable-flex-matching t
      ido-create-new-buffer 'always
      ido-use-filename-at-point 'guess
      ido-max-prospects 10
      ido-save-directory-list-file (expand-file-name "ido.hist" devesine-savefile-dir)
      ido-file-extension-order '(".org" ".txt" ".py" ".sls" ".emacs" ".ini" ".cfg" ".el" ".xml")
      ido-default-file-method 'selected-window) ; default raise-frame

;; auto-completion in minibuffer
(icomplete-mode +1)

(set-default 'imenu-auto-rescan t)


;; use ls-lisp for dired on local files
(require 'ls-lisp)
(setq ls-lisp-use-insert-directory-program nil)

;; clean out old buffers from buffer list
(when (require 'midnight nil 'noerror)
  (setq clean-buffer-list-delay-special (* 6 3600))
;;  ;(cancel-timer 'midnight-timer)
)

;; Shorter initial *scratch* message
(setq initial-scratch-message (purecopy "\
;; Lisp Interaction Buffer:

")) 

;; enable dired-find-alternate-file (bound to a)
(put 'dired-find-alternate-file 'disabled nil)
(setq dired-listing-switches "-alhFgG")

;; ediff - don't start another frame
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; saner regex syntax
(require 're-builder)
(setq reb-re-syntax 'string)

(require 'eshell)
(setq eshell-directory-name
      (expand-file-name "eshell/" devesine-savefile-dir))
(unless (file-exists-p eshell-directory-name)
  (make-directory eshell-directory-name))

(setq semanticdb-default-save-directory
      (expand-file-name "semanticdb/" devesine-savefile-dir))
(unless (file-exists-p semanticdb-default-save-directory)
  (make-directory semanticdb-default-save-directory))

(require 'flymake-cursor)

;; deft configuration
(setq deft-directory (expand-file-name "~/Dropbox/notes")
      deft-extension "txt"
      deft-text-mode 'markdown-mode)
(require 'deft)

(require 'yasnippet)
(add-to-list 'yas-snippet-dirs devesine-snippets-dir)
(yas-global-mode 1)

(defvar devesine-diminish-modes
  '(     
    ("whitespace" . whitespace-mode)
    ("abbrev" . abbrev-mode)
    ("yasnippet" . yas-minor-mode)
    ("flycheck" . flycheck-mode)
    ("flyspell" . flyspell-mode)
    ("eldoc" . eldoc-mode)
    ("paredit" . paredit-mode)
    ("rainbow-mode" . rainbow-mode)
    ("rainbow-delimiters" . rainbow-delimiters-mode)))

(when (require 'diminish nil 'noerror)
(dolist (entry devesine-diminish-modes)
  (eval-after-load (car entry) `(diminish ',(cdr entry)))))

(provide 'devesine-ui)

;;; devesine-ui.el ends here
