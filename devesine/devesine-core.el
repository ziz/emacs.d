;;; devesine-core.el --- personal elisp functions and functionality
;;; Commentary:
;;; Code:

;; Ensure a server is running
;; --------------------------------------------------------
(if (file-exists-p
 (concat (getenv "TMPDIR") "emacs"
         (number-to-string
          (user-real-uid)) "/server"))
nil (server-start))


(setq backup-directory-alist '(("." . ".~"))) ;; keep backup files neatly out of the way in .~/


;; Font face
;; --------------------------------------------------------
(defun font-inconsolata ()
  "Switch default font to Inconsolata 14."
   (interactive)
   (set-face-attribute 'default nil :font "Inconsolata 14"))

;; Buffer indentation magic
(defun iwb ()
  "Indent the whole buffer."
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))


(defun add-to-alist (alist-var elt-cons &optional no-replace)
  "Add to the value of ALIST-VAR an element ELT-CONS if it isn't there yet.
If an element with the same car as the car of ELT-CONS is already present,
replace it with ELT-CONS unless NO-REPLACE is non-nil; if a matching
element is not already present, add ELT-CONS to the front of the alist.
The test for presence of the car of ELT-CONS is done with `equal'."
  (let ((existing-element (assoc (car elt-cons) (symbol-value alist-var))))
    (if existing-element
        (or no-replace
            (rplacd existing-element (cdr elt-cons)))
      (set alist-var (cons elt-cons (symbol-value alist-var))))))


;; Fix path - read from ~/.jdv_path

(defun read-lines (file)
  "Return a list of lines in FILE."
  (with-temp-buffer
    (insert-file-contents file)
    (split-string
     (buffer-string) "\n" t)
    ) )

(defun jdv-split-path-from-file (lines)
  "Read a colon-separated path from LINES and split it on the separator."
  (let ((linelist (if (listp lines) lines (list lines)) ))
    (split-string (car linelist) ":" t)))

(let ((path-file (expand-file-name "~/.jdv_path")))
  (when (file-exists-p path-file)
    (setenv "PATH"
	    (mapconcat 'identity 
		       (delete-dups 
			(append
			 (jdv-split-path-from-file (read-lines path-file))
			 (split-string (getenv "PATH") ":" t))) ":"))
    (setq exec-path
	  (delete-dups
	   (append
	    (split-string (getenv "PATH") ":" t)
	    exec-path)))))


(defun devesine-recompile-init ()
  "Byte-compile all your dotfiles again."
  (interactive)
  (byte-recompile-directory devesine-emacs-dir 0))

(provide 'devesine-core)

;;; devesine-core.el ends here
