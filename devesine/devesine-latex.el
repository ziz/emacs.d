;; devesine-latex.el
;;

(when (require 'tex-site nil 'noerror)
  
 	
(load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-PDF-mode t)
(setq-default TeX-master nil)

(setq LaTeX-command "latex -synctex=1")
(setq TeX-electric-escape nil)
(setq TeX-electric-sub-and-superscript nil)
(setq TeX-insert-braces t)
(setq TeX-newline-function (quote reindent-then-newline-and-indent))
(setq TeX-source-correlate-mode t)
(setq reftex-plug-into-AUCTeX t)

(add-hook 'LaTeX-mode-hook 'auto-fill-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

(setenv "PATH" (concat "/usr/texbin:" (getenv "PATH")))
(add-hook 'TeX-mode-hook
    (lambda ()
        (add-to-list 'TeX-output-view-style
            '("^pdf$" "."
              "/Applications/Skim.app/Contents/SharedSupport/displayline %n %o %b"))
	(add-to-list 'TeX-view-program-list
	    '("Skim" "/Applications/Skim.app/Contents/SharedSupport/displayline %n %o %b"))
	(add-to-list 'TeX-view-program-selection '(output-pdf "Skim"))

	))
)

(provide 'devesine-latex)
