;;; devesine-modes.el --- Mode-specific settings and mappings
;;; Commentary:
;;; Code:


(defvar devesine-auto-alist
  '(("\\.clj\\'" . clojure-mode)
    ("\\.coffee\\'" . coffee-mode)
    ("\\.css\\'" . css-mode)
    ("\\.el\\'" . emacs-lisp-mode)
    ("\\.js\\'" . js2-mode)
    ("\\.latex\\'" . LaTeX-mode)
    ("\\.lisp\\'" . lisp-mode)
    ("\\.markdown\\'" . markdown-mode)
    ("\\.md\\'" . markdown-mode)
    ("\\.php\\'" . php-mode)
    ("\\.pl\\'" . cperl-mode)
    ("\\.py\\'" . python-mode)
    ("\\.rb\\'" . ruby-mode)
    ("\\.rake\\'" . ruby-mode)
    ("Rakefile\\'" . ruby-mode)
    ("\\.gemspec\\'" . ruby-mode)
    ("\\.ru\\'" . ruby-mode)
    ("Gemfile\\'" . ruby-mode)
    ("Guardfile\\'" . ruby-mode)
    ("Capfile\\'" . ruby-mode)
    ("\\.scss\\'" . scss-mode)
    ("\\.xml\\'" . nxml-mode)
    ("\\.yml\\'" . yaml-mode)
    ("\\.h\\'" . inform-maybe-mode)
    ("\\.inf\\'" . inform-mode)
    ("\\.ly\\'" . LilyPond-mode)
    ("\\.ily\\'" . LilyPond-mode)))

(dolist (entry devesine-auto-alist)
  (add-to-list 'auto-mode-alist entry))

;; Load all mode-specific files under the mode directory
;;(let ((mode-dir (concat devesine-core-dir "mode/")))
;;  (when (file-exists-p mode-dir)       
;;    (mapc 'load (directory-files mode-dir 't "^[^#].*el$"))))

;; Enable flymake for all files
(add-hook 'find-file-hook 'flycheck-mode)

;; enable company-mode globally
(global-company-mode)
(add-to-list 'company-backends 'company-anaconda)

;; pandoc mode
(require 'pandoc-mode)

;; Python mode customizations
;;; bind RET to py-newline-and-indent
(add-hook 'python-mode-hook '(lambda () 
     (define-key python-mode-map (kbd "RET") 'newline-and-indent)
     (set (make-local-variable 'paredit-space-for-delimiter-predicates) '((lambda (endp delimiter) nil)))
     (anaconda-mode)
))

;; C style customizations

;; (c-add-style "jdv" 
;;              '("gnu" 
;;                (c-hanging-braces-alist
;;                 (substatement-open after))
;;                (c-offsets-alist 
;;                 (substatement-open . 0))
;;                ))


;; (setq c-default-style
;;       '((java-mode . "java")
;;         (awk-mode . "awk")
;;         (other . "jdv")))


;; Python tabs - spaces only, 4 space indents

;(setq-default indent-tabs-mode t)
;(setq-default tab-width 4)

;(define-key (keymap-parent local-function-key-map) [S-tab] [backtab])
;(define-key (keymap-parent local-function-key-map) [S-iso-lefttab] [backtab])


;; languages

;;(require 'haml-mode)
;(require 'sass-mode) ; get scss-mode instead?
;(require 'php-mode) ; find a better php-mode?
;(load "jdv-php") ; and what the heck does this do?


;; file navigation

; (require 'textmate)
; (require 'peepopen)
; (textmate-mode)

;; Mode hooks

(when (require 'flyspell-lazy)
  (flyspell-lazy-mode 1))

(defun devesine-prog-mode-defaults ()
  "Default coding hooks."
  (flyspell-prog-mode))
(add-hook 'prog-mode-hook 'devesine-prog-mode-defaults)

(defun devesine-lisp-coding-defaults ()
  "Lisp coding hooks."
  (paredit-mode +1)
  (rainbow-delimiters-mode +1))
(add-hook 'emacs-lisp-mode-hook 'devesine-lisp-coding-defaults)
(add-hook 'cider-repl-mode-hook 'devesine-lisp-coding-defaults)
(add-hook 'clojure-mode-hook 'devesine-lisp-coding-defaults)

(defun devesine-emacs-lisp-mode-defaults ()
  "Emacs Lisp coding hooks."
  (make-local-variable 'after-save-hook)
  (add-hook 'after-save-hook
	    (lambda ()
	      (if (file-exists-p (concat buffer-file-name "c"))
		  (delete-file (concat buffer-file-name "c")))))
  (turn-on-eldoc-mode)
  (rainbow-mode +1)
  (idle-highlight-mode 1))
(add-hook 'emacs-lisp-mode-hook 'devesine-emacs-lisp-mode-defaults)

(define-key emacs-lisp-mode-map (kbd "M-.") 'find-function-at-point)

(defun devesine-ielm-mode-defaults ()
  "Ielm coding hooks."
  (turn-on-eldoc-mode))
(add-hook 'ielm-mode-hook 'devesine-ielm-mode-defaults)

(defun devesine-c-mode-common-defaults ()
  "C mode coding hooks."
  (setq indent-tabs-mode t)
  (setq c-basic-offset 4))
(add-hook 'c-mode-common-hook 'devesine-c-mode-common-defaults)

(defun devesine-makefile-mode-defaults ()
  "Makefile coding hooks."
  (setq indent-tabs-mode t))
(add-hook 'makefile-mode-hook 'devesine-makefile-mode-defaults)

(defun devesine-coffee-mode-defaults ()
  "Coffeescript coding hooks."
  (set (make-local-variable 'tab-width) 2)
  (set (make-local-variable 'coffee-tab-width) 2)
  (set (make-local-variable 'whitespace-action) '(auto-cleanup))
  (set (make-local-variable 'whitespace-style) '(face trailing space-before-tab indentation empty space-after-tab))
  (setq coffee-args-compile '("-c"))
  (setq coffee-debug-mode t)
  (electric-indent-mode -1)
  (idle-highlight-mode 1)
  (whitespace-mode 1)
  (define-key coffee-mode-map [(meta r)] 'coffee-compile-buffer)
  ;; Compile '.coffee' files on every save
  (and (file-exists-p (buffer-file-name))
       (file-exists-p (coffee-compiled-file-name))
       (coffee-cos-mode t)))
(add-hook 'coffee-mode-hook 'devesine-coffee-mode-defaults)

(defun devesine-css-mode-defaults ()
  "CSS mode coding hooks."
  (setq css-indent-offset 2)
  (rainbow-mode +1))
(add-hook 'css-mode-hook 'devesine-css-mode-defaults)

(defun devesine-scss-mode-defaults ()
  "SCSS coding hooks."
  (devesine-css-mode-defaults))
(add-hook 'scss-mode-defaults 'devesine-scss-mode-defaults)

(defun devesine-js2-mode-defaults ()
  "JavaScript coding hooks."
  (idle-highlight-mode 1))
(add-hook 'js2-mode-hook 'devesine-js2-mode-defaults)

(progn
  (defun devesine-php-mode-defaults ()
    "PHP mode coding hooks."
    (setq indent-tabs-mode t)
    (setq tab-width 4)
    (setq c-basic-offset 4))
  (add-hook 'php-mode-hook 'devesine-php-mode-defaults))

(progn 
  (defun devesine-inform-mode-defaults ()
    "Inform coding hooks."
    (turn-on-font-lock))
  (add-hook 'inform-mode-hook 'devesine-inform-mode-defaults))

(progn
  (defalias 'perl-mode 'cperl-mode)
  (defun devesine-perl-mode-defaults ()
    "Perl mode coding hooks."
    (setq cperl-indent-level 4
	  cperl-continued-statement-offset 8
	  cperl-font-lock t
	  cperl-electric-lbrace-space t
	  cperl-electric-parens nil
	  cperl-electric-linefeed nil
	  cperl-electric-keywords nil
	  cperl-info-on-command-no-prompt t
	  cperl-clobber-lisp-bindings t
	  cperl-lazy-help-time 3
	  )
    ; (setq cperl-hairy)
    ; (set-face-background 'cperl-array-face nil)
    ; (set-face-background 'cperl-hash-face nil)
    (setq cperl-invalid-face nil))
  (add-hook 'cperl-mode-hook 'devesine-perl-mode-defaults))


(progn
  ;;(require 'ruby-block)
  (add-to-list 'completion-ignored-extensions ".rbc")
  (defun devesine-ruby-mode-defaults ()
    "Ruby mode coding hooks."
             (inf-ruby-setup-keybindings)
         ;; turn off the annoying input echo in irb
         (setq comint-process-echoes t)
     (define-key ruby-mode-map (kbd "RET") 'newline-and-indent)
	 (idle-highlight-mode 1)
         ;; (ruby-block-mode t)
         (ruby-end-mode +1)
         (ruby-tools-mode +1)
         ;; CamelCase aware editing operations
         (subword-mode +1)
         ;; bind yari in the local keymap
         (local-set-key (kbd "C-h r") 'yari))
  (add-hook 'ruby-mode-hook 'devesine-ruby-mode-defaults))


(progn
 (push '("<\\?xml" . nxml-mode) magic-mode-alist)

(progn
  (defun use-monospace ()
  	  "Switch the current buffer to a monospace font."
  	    (face-remap-add-relative 'default '(:family "Monospace")))

  (add-hook 'dired-mode-hook 'use-monospace))

 ;; pom files should be treated as xml files
 (add-to-list 'auto-mode-alist '("\\.pom$" . nxml-mode))

 (setq nxml-child-indent 4)
 (setq nxml-attribute-indent 4)
 (setq nxml-auto-insert-xml-declaration-flag nil)
 (setq nxml-bind-meta-tab-to-complete-flag t)
 (setq nxml-slash-auto-complete-flag t))

(add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)
(setq nrepl-hide-special-buffers t)

(provide 'devesine-modes)

;;; devesine-modes.el ends here
