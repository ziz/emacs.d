(deftheme devesine
  "Created 2012-09-28.")

(custom-theme-set-variables
 'devesine
 '(dired-listing-switches "-alhFgG")
 '(ido-enable-flex-matching t)
 '(ls-lisp-verbosity (quote (uid)))
 '(tramp-default-host "lambda.ziz.org")
 '(tramp-default-method "scpcm")
 '(frame-background-mode 'dark)
 '(show-paren-mode t))

(custom-theme-set-faces
 'devesine
 ;'(default ((t (:stipple nil :background "Black" :foreground "#ccffcc" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "apple" :family "Inconsolata")))))
 '(default ((t (:stipple nil :background "#002b36" :foreground "#839496" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 140 :width normal :foundry "apple" :family "Inconsolata")))))

(provide-theme 'devesine)
