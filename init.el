;; init.el --- basic entrypoint

(require 'cl)	

;; Handle .emacs.d / site-lisp / vendor path loading
;; --------------------------------------------------------

(unless (boundp 'user-emacs-directory)
      (defvar user-emacs-directory "~/.emacs.d/"
        "Directory beneath which additional per-user Emacs-specificfiles are placed. Various programs in Emacs store information in this directory. Note that this should end with a directory separator. See also 'locate-user-emacs-file'."))

;; (add-to-list 'load-path user-emacs-directory)

(defvar devesine-emacs-dir (if load-file-name (file-name-directory (expand-file-name load-file-name)) user-emacs-directory)
  "The root directory of the emacs configuration.")
(defvar devesine-core-dir (expand-file-name "devesine" devesine-emacs-dir)
  "This directory holds basic (local) functionality and extensions for emacs configuration.")
(defvar devesine-savefile-dir (expand-file-name "savefile" devesine-emacs-dir)
  "This directory holds automatically generated save/history files.")
(defvar devesine-snippets-dir (expand-file-name "snippets" devesine-emacs-dir)
  "This directory holds additional yasnippet bundles.")
(defvar devesine-themes-dir (expand-file-name "themes" devesine-emacs-dir)
  "This directory holds additional themes.")


(let ((user-emacs-load-paths '("site-lisp/" "vendor/")))  
  (while user-emacs-load-paths
    (print (car user-emacs-load-paths))
    (let ((default-directory (concat user-emacs-directory
				     (convert-standard-filename (car user-emacs-load-paths)))))
	  (when (file-exists-p default-directory)
		(normal-top-level-add-to-load-path '("."))
		(normal-top-level-add-subdirs-to-load-path)))
    (setq user-emacs-load-paths (cdr user-emacs-load-paths))))

(unless (file-exists-p devesine-savefile-dir)
  (make-directory devesine-savefile-dir))

(add-to-list 'load-path devesine-core-dir)

(add-to-list 'custom-theme-load-path devesine-themes-dir)
(add-to-list 'custom-theme-load-path
	     (concat user-emacs-directory (convert-standard-filename "vendor/emacs-color-theme-solarized")))

(set-frame-parameter nil 'background-mode 'dark)
(set-terminal-parameter nil 'background-mode 'dark)
(load-theme 'solarized t)
(add-hook 'after-make-frame-functions
	  (lambda (frame)
	    (let ((mode (if (display-graphic-p frame) 'dark 'dark)))
	      (set-frame-parameter frame 'background-mode mode)
	      (set-terminal-parameter frame 'background-mode mode))
	    (enable-theme 'solarized)))
;;(unless (featurep 'aquamacs)
;;	(load-theme 'devesine t))

;; ELPA package loading
(require 'devesine-packages)

;; Functions and definitions
(require 'devesine-core)

;; Vendor package loading
;;(require 'devesine-tramp)
(require 'devesine-latex)

;; Configuration
(require 'devesine-ui)
(require 'devesine-modes)
(require 'devesine-bindings)

(when (eq system-type 'darwin)
  (require 'devesine-osx))

(setq custom-file (concat devesine-emacs-dir "emacs-custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))
